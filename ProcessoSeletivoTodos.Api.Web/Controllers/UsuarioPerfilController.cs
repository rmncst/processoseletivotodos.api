﻿using AutoMapper;
using ProcessoSeletivoTodos.Domain.Entities;
using ProcessoSeletivoTodos.Domain.Services.Interfaces;
using ProcessoSeletivoTodos.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProcessoSeletivoTodos.Web.Controllers
{
    public class UsuarioPerfilController : ApiController
    {
        protected readonly IUsuarioPerfilService ServiceUserProfile;

        public UsuarioPerfilController(IUsuarioPerfilService service)
        {
            ServiceUserProfile = service;
        }

        [HttpGet]
        // GET: api/UsuarioPerfil
        public IEnumerable<UsuarioPerfilViewModel> Get()
        {
            return ServiceUserProfile.FindAll().Select(s => Mapper.Map<UsuarioPerfil, UsuarioPerfilViewModel>(s));
        }

        [HttpGet]
        // GET: api/UsuarioPerfil
        public IEnumerable<UsuarioPerfilViewModel> GetAny(int iduser, int idprofile)
        {
            return ServiceUserProfile.FindAny(iduser,idprofile).Select(s => Mapper.Map<UsuarioPerfil, UsuarioPerfilViewModel>(s));
        }

        [HttpGet]
        // GET: api/UsuarioPerfil/5
        public UsuarioPerfilViewModel Get(int userid, int profileid)
        {
            return Mapper.Map<UsuarioPerfil, UsuarioPerfilViewModel>(ServiceUserProfile.Find(userid,profileid));
        }

        [HttpPost]
        // POST: api/UsuarioPerfil
        public UsuarioPerfilViewModel Post(UsuarioPerfilViewModel model)
        {
            var entity  = new UsuarioPerfil
            {
                 ID_PERFIL = model.PerfilId
                ,ID_USUARIO = model.UsuarioId
                ,ATIVO = true
            };

            ServiceUserProfile.Add(entity);

            return Mapper.Map<UsuarioPerfil, UsuarioPerfilViewModel>(entity);
        }

        [HttpPut]
        // PUT: api/UsuarioPerfil/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpDelete]
        // DELETE: api/UsuarioPerfil/5
        public UsuarioPerfilViewModel Delete(int iduser, int idprofile)
        {
            var entity = ServiceUserProfile.Find(iduser, idprofile);
            ServiceUserProfile.Delete(entity);

            return Mapper.Map<UsuarioPerfil, UsuarioPerfilViewModel>(entity);
        }
    }
}
