﻿using AutoMapper;
using ProcessoSeletivoTodos.Domain.Entities;
using ProcessoSeletivoTodos.Domain.Services.Interfaces;
using ProcessoSeletivoTodos.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProcessoSeletivoTodos.Web.Controllers
{
    public class PerfilController : ApiController
    {
        protected readonly IPerfilService Service;

        public PerfilController(IPerfilService serv)
        {
            Service = serv;
        }

        // GET: api/Perfil
        public IEnumerable<PerfilViewModel> Get()
        {
            return Service.FindAll().Select(s => Mapper.Map<Perfil, PerfilViewModel>(s));
        }

        // GET: api/Perfil/5
        public PerfilViewModel Get(int id)
        {
            return  Mapper.Map<Perfil, PerfilViewModel>(Service.Find(id));
        }

        // POST: api/Perfil
        public PerfilViewModel Post(PerfilViewModel value)
        {
            var entity = Mapper.Map<PerfilViewModel, Perfil>(value);
            Service.Add(entity);

            return Mapper.Map<Perfil, PerfilViewModel>(entity);
        }

        // PUT: api/Perfil/5
        public PerfilViewModel Put(PerfilViewModel value)
        {
            var entity = Mapper.Map<PerfilViewModel, Perfil>(value);
            Service.Update(entity);

            return Mapper.Map<Perfil, PerfilViewModel>(entity);
        }

        // DELETE: api/Perfil/5
        public void Delete(int id)
        {
            Service.Delete(Service.Find(id));
        }
    }
}
