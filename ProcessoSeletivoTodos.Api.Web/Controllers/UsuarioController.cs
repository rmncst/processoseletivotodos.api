﻿using AutoMapper;
using ProcessoSeletivoTodos.Domain.Entities;
using ProcessoSeletivoTodos.Domain.Services.Interfaces;
using ProcessoSeletivoTodos.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProcessoSeletivoTodos.Web.Controllers
{
    public class UsuarioController : ApiController
    {
        protected readonly IUsuarioService UsuarioService;

        public UsuarioController(IUsuarioService service)
        {
            UsuarioService = service;
        }

        [HttpGet]
        public IEnumerable<UsuarioViewModel> GetAll()
        {
            return UsuarioService.FindAll().Select(s => Mapper.Map<Usuario, UsuarioViewModel>(s));
        }

        [HttpGet]
        public UsuarioViewModel Get(int id)
        {
            var entity = UsuarioService.Find(id);
            return Mapper.Map<Usuario, UsuarioViewModel>(entity);
        }

        [HttpPost]
        public UsuarioViewModel Add(UsuarioViewModel entity)
        {
            entity.DataCadastro = DateTime.Now;
            var insert = Mapper.Map<UsuarioViewModel, Usuario>(entity);
            
            UsuarioService.Add(insert);
            entity.UsuarioId = insert.ID_USUARIO;

            return entity;
        }

        [HttpDelete]
        public UsuarioViewModel Delete(int id)
        {
            var entity = UsuarioService.Find(id);
            UsuarioService.Delete(entity);

            return Mapper.Map<Usuario, UsuarioViewModel>(entity);
        }


        [HttpPut]
        public UsuarioViewModel Update(UsuarioViewModel entity)
        {
            var updated = UsuarioService.Find(entity.UsuarioId);
            updated.ATIVO = entity.Ativo;
            updated.EMAIL = entity.Email;
            updated.LOGIN = entity.Login;
            
            UsuarioService.Update(updated);

            return Mapper.Map<Usuario, UsuarioViewModel>(updated);
        }

    }
}
