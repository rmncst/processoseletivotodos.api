﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessoSeletivoTodos.Web.ViewModel
{
    public class PerfilViewModel
    {
        public int PerfilId { get; set; }
        public string Descricao { get; set; }

    }
}