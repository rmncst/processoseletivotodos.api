﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessoSeletivoTodos.Web.ViewModel
{
    public class UsuarioPerfilViewModel
    {
        public int UsuarioId { get; set; }
        public int PerfilId { get; set; }
        public virtual PerfilViewModel Perfil { get; set; }
        public virtual UsuarioViewModel Usuario { get; set; }
    }
}