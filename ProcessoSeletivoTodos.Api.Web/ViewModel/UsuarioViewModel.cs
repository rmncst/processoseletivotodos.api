﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessoSeletivoTodos.Web.ViewModel
{
    public class UsuarioViewModel
    {
        public int UsuarioId { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataCadastro { get; set; }
        public string DataCadastroString { get; set; }
    }
}