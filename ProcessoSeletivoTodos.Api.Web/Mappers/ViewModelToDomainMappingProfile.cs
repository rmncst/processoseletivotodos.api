﻿using AutoMapper;
using ProcessoSeletivoTodos.Domain.Entities;
using ProcessoSeletivoTodos.Web.ViewModel;

namespace ProcessoSeletivoTodos.Web.Mappers
{
    internal class ViewModelToDomainMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<UsuarioViewModel, Usuario>().ConstructUsing(d => new Usuario
            {
                  ATIVO = d.Ativo
                , DT_INCLUSAO = d.DataCadastro
                , EMAIL = d.Email
                , ID_USUARIO = d.UsuarioId
                , LOGIN = d.Login
                , SENHA = d.Senha
            });
            
            CreateMap<PerfilViewModel, Perfil>().ConstructUsing(d => new Perfil
            {
                ID_PERFIL = d.PerfilId
                ,NOME = d.Descricao
            });

            
            CreateMap<UsuarioPerfilViewModel, UsuarioPerfil>().ConstructUsing(d => new UsuarioPerfil
            {
                 ID_PERFIL = d.PerfilId
                ,ID_USUARIO = d.UsuarioId
            });
        }
    }
 }