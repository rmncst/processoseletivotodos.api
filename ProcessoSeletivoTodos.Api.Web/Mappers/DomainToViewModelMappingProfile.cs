﻿using AutoMapper;
using ProcessoSeletivoTodos.Domain.Entities;
using ProcessoSeletivoTodos.Web.ViewModel;

namespace ProcessoSeletivoTodos.Web.Mappers
{
    internal class DomainToViewModelMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Usuario, UsuarioViewModel>().ConstructUsing( d => new UsuarioViewModel
            {
                 Ativo = d.ATIVO
                ,Login = d.LOGIN
                ,DataCadastro = d.DT_INCLUSAO
                ,Email = d.EMAIL
                ,UsuarioId = d.ID_USUARIO    
                ,Senha = d.SENHA
            });

            CreateMap<Perfil , PerfilViewModel>().ConstructUsing(d => new PerfilViewModel
            {
                Descricao = d.NOME
                ,PerfilId = d.ID_PERFIL
            });

            CreateMap<UsuarioPerfil, UsuarioPerfilViewModel>().ConstructUsing(d => new UsuarioPerfilViewModel
            {
                 PerfilId = d.ID_PERFIL
                ,UsuarioId = d.ID_USUARIO
            });
        }
    }
}