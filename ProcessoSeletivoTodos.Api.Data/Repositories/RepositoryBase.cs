﻿using ProcessoSeletivoTodos.Data.Context;
using ProcessoSeletivoTodos.Domain.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Data.Repositories
{
    public class RepositoryBase<TEntiy> : IRepositoryBase<TEntiy> where TEntiy : class
    {
        protected readonly ProcessoSeletivoTodosContext Context;

        public RepositoryBase()
        {
            Context = new ProcessoSeletivoTodosContext();
        }   

        public void Add(TEntiy entity)
        {
            Context.Set<TEntiy>().Add(entity);
            Context.SaveChanges();
        }

        public void Delete(TEntiy entity)
        {
            Context.Set<TEntiy>().Remove(entity);
            Context.SaveChanges();
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public IEnumerable<TEntiy> Find(Func<TEntiy, bool> func)
        {
            return Context.Set<TEntiy>().Where(func).ToList();
        }

        public TEntiy Find(params object[] keys)
        {
            return Context.Set<TEntiy>().Find(keys);
        }

        public IEnumerable<TEntiy> FindAll()
        {
            return Context.Set<TEntiy>().ToList();
        }

        public void Update(TEntiy entity)
        {
            Context.Entry<TEntiy>(entity).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
