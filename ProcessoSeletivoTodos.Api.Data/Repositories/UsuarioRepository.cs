﻿using ProcessoSeletivoTodos.Domain.Entities;
using ProcessoSeletivoTodos.Domain.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Data.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario> , IUsuarioRepository
    {
    }
}
