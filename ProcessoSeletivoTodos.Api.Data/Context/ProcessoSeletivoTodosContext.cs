﻿using ProcessoSeletivoTodos.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Data.Context
{
    public class ProcessoSeletivoTodosContext : DbContext
    {
        public ProcessoSeletivoTodosContext() : base("name=ProcessoSeletivoTODOS") { }

        public virtual DbSet<Perfil> Perfis { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<UsuarioPerfil> UsuariosPerfis { get; set; }
        public virtual DbSet<OperacaoUsuario> OperacaoUsuario { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Perfil>()
                .Property(e => e.NOME)
                .IsUnicode(false);

            modelBuilder.Entity<Perfil>()
                .HasMany(e => e.USUARIO_PERFIL)
                .WithRequired(e => e.PERFIL)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.LOGIN)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.USUARIO_PERFIL)
                .WithRequired(e => e.USUARIO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuario>()
                .Property(d => d.DT_INCLUSAO)
                .HasColumnType("datetime");
                
        }
    }
}
