namespace ProcessoSeletivoTodos.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("todos.USUARIO")]
    public partial class Usuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuario()
        {
            USUARIO_PERFIL = new HashSet<UsuarioPerfil>();
        }

        [Key]
        public int ID_USUARIO { get; set; }

        [Required]
        [StringLength(120)]
        public string LOGIN { get; set; }

        [Required]
        [StringLength(120)]
        public string SENHA { get; set; }

        [StringLength(220)]
        public string NOME { get; set; }

        [Required]
        [StringLength(220)]
        public string EMAIL { get; set; }

        
        public bool ATIVO { get; set; }

        public DateTime DT_INCLUSAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsuarioPerfil> USUARIO_PERFIL { get; set; }
    }
}
