﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Domain.Entities
{

    [Table("todos.OPERACAO_USUARIO")]
    public class OperacaoUsuario
    {
        [Key]
        public int ID_OPERACAO_ACESSO { get; set; }

        [Required]
        public DateTime DT_LOG { get; set; }

        [Required]
        public int ID_USUARIO { get; set; }

        [Required]
        [StringLength(1)]
        public string TIPO { get; set; }
    }
}
