namespace ProcessoSeletivoTodos.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("todos.USUARIO_PERFIL")]
    public partial class UsuarioPerfil
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_USUARIO { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_PERFIL { get; set; }

        public bool ATIVO { get; set; }

        public virtual Perfil PERFIL { get; set; }

        public virtual Usuario USUARIO { get; set; }
    }
}
