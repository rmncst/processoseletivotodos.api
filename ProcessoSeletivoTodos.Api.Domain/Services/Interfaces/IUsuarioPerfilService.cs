﻿using ProcessoSeletivoTodos.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Domain.Services.Interfaces
{
    public interface IUsuarioPerfilService : IServiceBase<UsuarioPerfil>
    {

        /// <summary>
        /// Retorna todos os registro relacionados ao parâmetros.
        /// Caso queira filtrar apenas por uma das chaves, a outra deve ser informada com o valor 0.
        /// </summary>
        /// <param name="idser"></param>
        /// <param name="idprofile"></param>
        /// <returns></returns>
        IEnumerable<UsuarioPerfil> FindAny(int idser, int idprofile = 0);
    }
}
