﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Domain.Services.Interfaces
{
    public interface IServiceBase<TEntity> : IDisposable
    {
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);

        TEntity Find(params object[] keys);
        IEnumerable<TEntity> Find(Func<TEntity, bool> func);
        IEnumerable<TEntity> FindAll();
    }
}
