﻿using ProcessoSeletivoTodos.Domain.Entities;
using ProcessoSeletivoTodos.Domain.Repositories.Interfaces;
using ProcessoSeletivoTodos.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Domain.Services.Classes
{
    public class UsuarioService : ServiceBase<Usuario> , IUsuarioService
    {
        public UsuarioService(IUsuarioRepository repo) : base(repo) { }
    }
}
