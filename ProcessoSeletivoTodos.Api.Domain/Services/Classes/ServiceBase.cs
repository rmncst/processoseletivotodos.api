﻿using ProcessoSeletivoTodos.Domain.Repositories.Interfaces;
using ProcessoSeletivoTodos.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Domain.Services.Classes
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity>
    {
        protected IRepositoryBase<TEntity> Repository;

        public ServiceBase(IRepositoryBase<TEntity> repo) {
            Repository = repo;
        }

        public void Add(TEntity entity)
        {
            Repository.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            Repository.Delete(entity);
        }

        public void Dispose()
        {
            Repository.Dispose();
        }

        public IEnumerable<TEntity> Find(Func<TEntity, bool> func)
        {
            return Repository.Find(func);
        }

        public TEntity Find(params object[] keys)
        {
            return Repository.Find(keys);
        }

        public IEnumerable<TEntity> FindAll()
        {
            return Repository.FindAll();
        }

        public void Update(TEntity entity)
        {
            Repository.Update(entity);
        }
    }
}
