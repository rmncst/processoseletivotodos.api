﻿using ProcessoSeletivoTodos.Domain.Entities;
using ProcessoSeletivoTodos.Domain.Repositories.Interfaces;
using ProcessoSeletivoTodos.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Domain.Services.Classes
{
    public class UsuarioPerfilService : ServiceBase<UsuarioPerfil> , IUsuarioPerfilService
    {
        public UsuarioPerfilService(IUsuarioPerfilRepository repo) : base(repo) { }

        public IEnumerable<UsuarioPerfil> FindAny(int idser, int idprofile = 0)
        {
            if (idprofile == 0)
                return Repository.Find(d => d.ID_USUARIO == idser);
            else
                if (idser == 0)
                    return Repository.Find(d => d.ID_PERFIL == idprofile);
                else
                    return Repository.Find(d => d.ID_PERFIL == idprofile && d.ID_USUARIO == idser);
        }
    }
}
