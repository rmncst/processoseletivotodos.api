﻿using ProcessoSeletivoTodos.Domain.Entities;
using ProcessoSeletivoTodos.Domain.Repositories.Interfaces;
using ProcessoSeletivoTodos.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Domain.Services.Classes
{
    public class PerfilService : ServiceBase<Perfil> , IPerfilService
    {
        public PerfilService(IPerfilRepository repo) : base(repo) { }
    }
}
