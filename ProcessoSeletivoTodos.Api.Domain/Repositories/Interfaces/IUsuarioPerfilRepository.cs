﻿using ProcessoSeletivoTodos.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessoSeletivoTodos.Domain.Repositories.Interfaces
{
    public interface IUsuarioPerfilRepository : IRepositoryBase<UsuarioPerfil>
    {

    }
}
