Repositório referente a aplicação WEB API solicitada no processo seletivo.

**Considerações**

O banco de dados da aplicação, foi criado originalmente via linha de comando SQL, e o domínio da aplicação gerado por engenharia reversa. Porém, caso queira executar a aplicação antes de criar o banco pelo script enviado no outro repositório, também funcionará normalmente. 

Porém, é preciso lembrar que as demais estruturas de banco de dados, procedures e trigger's, precisam ser criadas via scritps ( que residem no repositório https://bitbucket.org/rmncst/processoseletivotodos.scripts ).

Caso haja algum problema com os scripts, recriar o banco via script ( Caso não tenha sido feito ).